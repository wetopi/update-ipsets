FROM debian:jessie

MAINTAINER joan@inte.es

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
 && apt-get install -y -q --no-install-recommends \
            apt-utils build-essential git make gcc autoconf autogen automake pkg-config \
            curl ca-certificates ipset \
            inetutils-traceroute unzip \
            \
 && cd /tmp \
    \
 && echo "===> download iprange and firehol from github" \
 && git clone https://github.com/firehol/iprange.git iprange.git \
 && git clone https://github.com/firehol/firehol.git firehol.git \
    \
 && echo "===> install iprange" \
 && cd iprange.git \
 && ./autogen.sh \
 && ./configure --prefix=/usr CFLAGS="-march=native -O3" --disable-man \
 && make && make install \
    \
 && echo "===> install update-ipsets" \
 && cd ../firehol.git \
 && ./autogen.sh \
 && ./configure --prefix=/usr --sysconfdir=/etc \
                --disable-firehol -disable-fireqos --disable-link-balancer \
                --disable-vnetbuild --disable-man --disable-doc \
 && make && make install \
 && cp /tmp/firehol.git/contrib/*.sh /usr/lib/firehol/ \
    \
    \
 && apt-get -y purge apt-utils build-essential git make gcc autoconf autogen automake pkg-config \
 && apt-get -y autoremove \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/*

# About this Repo

Docker Image used to build update-ipsets and iprange

update-ipsets can be used to maintain/update ipset sources based on public IP lists like [iplists.firehol.org](http://iplists.firehol.org/)


### Why we need this image? update-ipsets needs to work side by side with iptables in your kernel?

This image is used only to compile the last iprange and firehol's update-ipsets script's
The idea is to avoid poluting production servers with libs and packages not required. 

NOTE: you can use this image to build ipsets in a container and then install them manually using `usr/lib/firehol/ipset-apply.sh`

### How to build it locally?

```sh
docker build -t wetopi/update-ipsets .
```

### Build Docker hub image

```sh
git tag 3.0.1
git push bitb master && git push bitb --tags
```

## Installing update-ipsets and iprange in our server

```sh
docker run -i --rm -v /usr:/artifacts wetopi/update-ipsets:3.0.1 bash << COMMANDS
cp /usr/sbin/update-ipsets /artifacts/sbin/
cp /usr/bin/iprange /artifacts/bin/
cp -R /usr/lib/firehol /artifacts/lib/
COMMANDS
```

NOTE: `/usr/lib/firehol` contains some scripts functions and utils.

## Using update-ipsets

[Wiki Downloading IP lists](https://github.com/firehol/blocklist-ipsets/wiki/Downloading-IP-Lists)

